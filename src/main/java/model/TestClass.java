package model;

public class TestClass {

    private final String message;

    public TestClass() {
        this.message = "This is a test class, to check how to make merge request in GitLab and close it";
    }

    public String getMessage(){
        return this.message;
    }
}
